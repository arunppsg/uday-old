# Table of Contents

<!-- toc -->
- [Contributing to Uday](#contributing-to-uday)
- [Getting Started](#getting-started)
- [Coding Convention](#coding-convention)
- [Documentation Conventions](#documentation-conventions)
<!-- tocstop -->

## Getting Started

Here are some tips to get started:

1. Fork the [Uday](https://gitlab.com/arunppsg/uday/) repository
and clone the forked repository

```bash
git clone https://gitlab.com/YOUR-USERNAME/uday.git
cd uday
```

1.1. If you already have Uday from source, update it by running
```bash
git fetch upstream
git rebase upstream/master
```

2. Install Udahy in develop mode

```bash
python setup.py develop
```

This mode will symlink the Python files from current local source tree into
the Python install. Hence, if you modify a Python file, you do not need to
reinstall Uday for the changes to reflect again.

Some other tips:
* All contributions must pass unit tests.

## Coding Conventions

Uday uses these tools and styles to keep the code healthy:
* [YAPF](https://github.com/google/yapf) (code format)
* [Flake8](https://flake8.pycqa.org/en/latest/) (code style check)
* [mypy](http://mypy-lang.org/) (type check)
* [doctest](https://docs.python.org/3/library/doctest.html) (interactive examples)
* [pytest](https://docs.pytest.org/en/6.2.x/index.html) (unit testing)

Before making a PR, please check your codes using them.
You can confirm how to check your codes from [Coding Conventions]().


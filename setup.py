import setuptools

setuptools.setup(
    name='uday',
    version='0.0.1',
    url='https://gitlab.com/arunppsg/uday',
    maintainer='Uday contributors',
    classifiers=[
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Intended Audience :: Network Analysts',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.8',
    ], 
    description='Malware Detection Framework',
    keywords=[
        'uday',
        'malware',
        'security',
    ],
    install_requires=[
        'scapy',
        'pandas',
    ],
    packages=setuptools.find_packages(),
    python_requires=">=3.8",
    project_urls={
        'Documentation': 'https://gitlab.com/arunppsg/uday/-/tree/main/doc',
        'Bug Tracker': 'https://gitlab.com/arunppsg/uday/-/issues',
    })

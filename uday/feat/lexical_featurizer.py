"""
Returns a lexically featurized string
"""
import numpy as np
from typing import Dict

from uday.feat import Featurizer


class LexicalFeaturizer(Featurizer):
    """Given a malicious DNS, the featurizer computes lexical features
    of the DNS. Whatever the 
    entity is, the lexical featurizer computes its features.

    """
    def __init__(self):
        self.symbols = "+.=?-!@#$%^&*(){}[];:'<>,/|\\\""
        self.puncs = "%?-=."
        # TODO(arun) can above two be combined?

    def _get_length_related_features(self, query):
        len_features = {}
        len_features['query_len'] = len(query)
        tokens = query.split('.')
        tokens_len = list(map(len, tokens))
        len_features['avg_token_len'] = np.mean(tokens_len)
        return len_features

    def _get_count_related_features(self, query):
        count_features = {}
        digit_count = sum([char.isdigit() for char in query])
        symbol_count = sum([char in self.symbols for char in query])
        alpha_count = sum([char.isalpha() for char in query]) 
        count_features['alpha_count'] = alpha_count
        count_features['symbol_count'] = symbol_count
        count_features['digit_count'] = digit_count
        
        puncs = self.puncs
        for punc in puncs:
            count_features[('count'+punc)] = query.count(punc)

        query_len = len(query)
        smoothing_factor = 1/3
        digit_count += smoothing_factor
        alpha_count += smoothing_factor
        symbol_count += smoothing_factor
        # Smoothing factor is used to offset queries
        # without digit/alphabets/symbols
        count_features['digit_ratio'] = digit_count / (query_len + 1)
        count_features['alpha_ratio'] = alpha_count / (query_len + 1)
        count_features['symbol_ratio'] = symbol_count / (query_len + 1)
        count_features['alpha_to_digit'] = alpha_count / digit_count
        count_features['alpha_to_symbol'] = alpha_count / symbol_count

        return count_features

    def _get_entropy_related_features(self, query) -> Dict:
        """Malicious queries have a higher entropy. Example, 
        www.google.com might be written as www.go0gle.c0m. Entropy
        can be a good feature in detecting malicious queries.
        """
        entropy_features = {}
        query = list(query)
        labels, counts = np.unique(query, return_counts=True)
        counts = counts / np.sum(counts)
        ent = sum(counts * np.log(counts))
        entropy_features['entropy'] = ent
        return entropy_features

    def _get_character_continuity_rate(self, query):
        """Legitimate websites prefer to use a simple and memorable
           domain name even if they need to pay more money. But
           malicious website owners frequently change the domain name
           so they are unlikely to pay more money for buying a better
           domain name. Based on this idea, we design the character
           continuity rate of domain name.
           First we categorize the character to letter, digit and symbol.
           The domain name will be split by the connection of different
           character category, and the sum of the longest token length
           of each character type will divide by the domain length. For
           example, the continue rate of “abc567-gt” = (3 + 3 + 1) /9 = 0.77.
          
         """
        # TODO Make the above brief
        ccr = {}
        symbols = self.symbols 
        longest_alpha, longest_digit, longest_symbol = 0, 0, 0
        prev_char = query[0]
        alpha, digit, symbol = query[0].isalpha(), query[0].isdigit(), query[0] in symbols

        for i in range(1, len(query)):
            char = query[i]
            if char.isalpha() and prev_char.isalpha():
                alpha += 1
            elif not char.isalpha() and prev_char.isalpha():
                longest_alpha = max(longest_alpha, alpha)
            elif char.isalpha() and not prev_char.isalpha():
                alpha = 1
        
            if char.isdigit() and prev_char.isdigit():
                digit += 1
            elif not char.isdigit() and prev_char.isdigit():
                longest_digit = max(longest_digit, digit)
            elif char.isdigit() and not prev_char.isdigit():
                digit = 1
         
            if char in symbols and prev_char in symbols:
                symbol += 1
            elif char not in symbols and prev_char in symbols:
                longest_symbol = max(longest_symbol, symbol)
            elif char in symbols and prev_char not in symbols:
                symbol = 1
            prev_char = char

        char_cont_rate = (longest_symbol + longest_digit +
                          longest_alpha) / len(query)
        ccr['ccr'] = char_cont_rate
        return ccr 

    def _get_features(self, sample: str):
        features = {}
        len_features = self._get_length_related_features(sample)
        count_features = self._get_count_related_features(sample)
        entropy_features = self._get_entropy_related_features(sample)
        ccr_features = self._get_character_continuity_rate(sample)
        features = {**len_features, **count_features, **entropy_features,
                    **ccr_features}
        return features

    def get_feature_names(self):
        """This method is used to get the name of the features 
        returned by featurizer. The featurizer returns an array or an iterable
        but it does not return a mapping on what the features are.
        To get what the features are, this method can be used.
        """
        dummy_query = "www.google1.com"
        features = self._get_features(dummy_query)
        return list(features.keys())

    def _featurize(self, sample: str):
        features = self._get_features(sample)
        return np.array(list(features.values()))

class URLLexicalFeaturizer(object):
    
    # This method should get features from the base class - Lexical analyzer as
    # well as add it's own attributes
    # One option is creating a LexicalAnalyzer object instance here.
    # Is there a better way? Like inheriting LexicalAnalyzer class?
    # Do we inherit the LexicalFeaturizer class or use an instance of the LF class?
    # Which option is correct ideally?
    def __init__(self):
        return

    def _featurize(self, sample):
        return

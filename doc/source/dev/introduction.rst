####################
Contributing to Uday
####################

Development Process - Summary
=============================

Here's the short summary of the process:

1. Setting up repository 

   * Go to `https://gitlab.com/arunppsg/uday
     <https://gitlab.com/arunppsg/uday>`_ and click the
     "fork" button to create your own copy of the project.

   * Clone the project to your local computer::

      git clone https://gitlab.com/YOUR-USERNAME/uday.git

   * Change the directory::

      cd uday 

   * Add the upstream repository::

      git remote add upstream https://gitlab.com/arunppsg/uday.git

   * Now, `git remote -v` will show two remote repositories named:

     - ``upstream``, which refers to the ``uday`` repository
     - ``origin``, which refers to your personal fork

2. Install ``uday`` in developmode::

    python setup.py develop

This mode will symlink the Python files from current local source tree 
into the Python install. Hence, if you modify a Python file, you do not 
need to reinstall `uday` again and again.

************
Installation
************

Installation steps:

* Clone the repository::

    git clone https://gitlab.com/arunppsg/uday.git

* Change the directory::

    cd uday

* Install it::

    python setup.py install
